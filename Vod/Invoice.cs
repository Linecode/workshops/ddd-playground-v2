using BuildingBlocks.Common;
using BuildingBlocks.Ddd;
using MyVod.Common.BuildingBlocks.EventSourcing;

namespace Vod;

// public record Error(string Name, string Reason);
//
// public static class Extensions
// {
//     public static Error AsError(this (string Name, string Reason) touple)
//         => new(touple.Name, touple.Reason);
// }

public class Invoice : Entity<Guid>, AggregateRoot<Guid>
{
    public Party Participant { get; private set; } = null!;

    public States Status { get; private set; } = States.Created;
    
    private List<InvoiceLine> _lines = new();
    public IReadOnlyList<InvoiceLine> Lines => _lines;

    public Result AddInvoiceLine(InvoiceLine line)
    {
        _lines.Add(line);

        // Invariant
        if (_lines.Sum(x => x.TotalPrice) <= 0)
            // return ("", "").AsErrorResult();
            return Result.Error(new Exception(""));
        
        return Result.Success();
    }

    public static Invoice Create(Party party)
    {
        var invoice = new Invoice
        {
            Participant = party,
            Id = Guid.NewGuid()
        };
        invoice.Raise(new Events.InvoiceCreatedForParty { InvoiceId = invoice.Id });

        return invoice;
    }

    public static class Events
    {
        public class InvoiceCreatedForParty : IDomainEvent
        {
            public Guid InvoiceId { get; init; }
        }
    }

    public enum States
    {
        Created,
        Processing,
        Accepted,
        Declined,
    }
}

public class InvoiceService
{
    public InvoiceEvents Decide(InvoiceCommands command, InvoiceV2 state)
    {
        return command switch
        {
            InvoiceCommands.CreateInvoice ci => Handle(ci, state),
            _ => throw new MissingMethodException()
        };
    }

    private InvoiceEvents Handle(InvoiceCommands.CreateInvoice command, InvoiceV2 state)
    {
        if (state is null)
        {
            var events = InvoiceV2.CreatedInvoice.Create(command.Party);
            
            return events;
        }

        return null;
    }
}

public abstract record InvoiceCommands
{
    public record CreateInvoice(Party Party) : InvoiceCommands;
    public record AddInvoiceLine(InvoiceLine Line, Guid InvoiceId) : InvoiceCommands;
}

public abstract record InvoiceEvents
{
    public record InvoiceCreated(Party Party, Guid InvoiceId) : InvoiceEvents;

    public record InvoiceLineAdded(InvoiceLine Line) : InvoiceEvents;
}

public abstract record InvoiceV2
{
    public record CreatedInvoice(Party party) : InvoiceV2
    {
        public ProcessingInvoice StartProcessing()
            => new ();
        
        public static InvoiceEvents Create(Party party)
        {
            var invoice = new CreatedInvoice(party);
            
            return new InvoiceEvents.InvoiceCreated(party, Guid.NewGuid());
        }
    }

    public record ProcessingInvoice : InvoiceV2
    {
        private List<InvoiceLine> _lines = new();
        public IReadOnlyList<InvoiceLine> Lines => _lines;

        public ProcessedInvoice StopProcessing() => new();

        public InvoiceEvents AddInvoiceLine(InvoiceLine line)
        {
            _lines.Add(line);

            // Invariant
            if (_lines.Sum(x => x.TotalPrice) <= 0)
                // return ("", "").AsErrorResult()
                throw new Exception("");
            
            return new InvoiceEvents.InvoiceLineAdded(line);
        }
    }

    public record ProcessedInvoice : InvoiceV2;
}

public class InvoiceLine : ValueObject<InvoiceLine>
{
    public string Name { get; private set; } = null!;
    public decimal Quantity { get; private set; }
    public decimal Price { get; private set; }
    public decimal Tax { get; private set; }
    
    public decimal TotalPrice => Quantity * Price * Tax;

    public static InvoiceLine Create(string name, decimal quantity, decimal price, decimal taxRate)
    {
        return new InvoiceLine
        {
            Name = name,
            Quantity = quantity,
            Price = price,
            Tax = price * taxRate 
        };
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Name;
        yield return Quantity;
        yield return Price;
        yield return Tax;
    }
}

// Party Archetype
public class Party : Entity<Guid>
{
    public string Name { get; private set; } = null!;
    public Nip Nip { get; private set; } = null!;
    public Address Address { get; private set; } = null!;

    public static Party Create(string name, Nip nip, Address address)
    {
        return new Party()
        {
            Name = name,
            Id = Guid.NewGuid(),
            Nip = nip,
            Address = address
        };
    }
}

public class Address : ValueObject<Address>
{
    public string Street { get; private set; }
    public string City { get; private set; }
    public string State { get; private set; }
    public string ZipCode { get; private set; }

    public static Address Create(string street, string city, string state, string zipCode)
    {
        return new Address
        {
            Street = street,
            City = city,
            State = state,
            ZipCode = zipCode
        };
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Street;
        yield return City;
        yield return State;
        yield return ZipCode;
    }
}

public class Nip : ValueObject<Nip>
{
    public string Value { get; private set; } = null!;

    public static Nip Create(string value)
    {
        // algorytm walidacji nipu 
        
        return new Nip { Value = value };
    }
    
    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return Value;
    }
}