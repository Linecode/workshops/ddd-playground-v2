using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Common.BuildingBlocks.EventSourcing;
using Newtonsoft.Json;

namespace Vod.DummyEventStore;

internal class Event
{
    public int Id { get; private set; }
    public DateTime OccuredOn { get; private set; }
    public string Payload { get; private set; }
    public string Type { get; private set; }
    public Guid AggregateId { get; private set; }
    public long Version { get; private set; }

    public Event(DateTime occuredOn, string payload, Guid aggregateId, string type, long version)
    {
        OccuredOn = occuredOn;
        Payload = payload;
        AggregateId = aggregateId;
        Type = type;
        Version = version;
    }
}

internal class EventTypeConfiguration : IEntityTypeConfiguration<Event>
{
    public void Configure(EntityTypeBuilder<Event> builder)
    {
        builder.ToTable("EventStore_Events");

        builder.HasKey(x => x.Id);

        builder.HasIndex(x => new {x.AggregateId, x.Version})
            .IsUnique();

        builder.HasIndex(x => x.AggregateId);

        builder.Property(x => x.Version)
            .IsConcurrencyToken();
    }
}


public class EventStore
{
    private readonly EventStoreContext _context;

    public EventStore(EventStoreContext context)
    {
        _context = context;
    }

    public async Task Save<T>(Guid aggregateId, T aggregate)
        where T : AggregateBase
    {
        var events = aggregate.GetUncommittedEvents();

        foreach (var @event in events)
        {
            _context.Events.Add(new Event(DateTime.UtcNow,
                JsonConvert.SerializeObject(@event,
                    new JsonSerializerSettings {ReferenceLoopHandling = ReferenceLoopHandling.Ignore}),
                aggregateId,
                @event.GetType().FullName,
                aggregate.Version));
        }

        await _context.SaveChangesAsync();
    }

    public async Task<T> Get<T>(Guid aggregateId)
        where T : AggregateBase
    {
        var stream = await _context.Events.Where(x => x.AggregateId == aggregateId)
            .OrderBy(x => x.OccuredOn).ToListAsync();

        var events = stream.Select(x =>
        {
            var type = Assembly.GetAssembly(typeof(EventStoreContext)).GetType(x.Type);
            var obj = JsonConvert.DeserializeObject(x.Payload, type);
            return (IDomainEvent)obj;
        });

        var aggregate = (T)Activator.CreateInstance(typeof(T));
        var type = aggregate.GetType();
        var prop = type.GetProperty("Id", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
        prop.SetValue(aggregate, aggregateId, null);

        foreach (var @event in events)
        {
            aggregate.Apply(@event);
        }

        return aggregate;
    }
}

public class EventStoreContext : DbContext
{
    internal DbSet<Event> Events { get; set; } = null!;

    public EventStoreContext(DbContextOptions<EventStoreContext> options)
        : base(options)
    {
    }
}