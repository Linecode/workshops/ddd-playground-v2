namespace BuildingBlocks.Ddd;

// ReSharper disable once InconsistentNaming
public interface AggregateRoot<out TIdentifier>
{ 
    TIdentifier Id { get; }
}