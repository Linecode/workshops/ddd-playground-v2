using System.Text.Json.Serialization;
using BuildingBlocks.Ddd;

namespace MyVod.Common.BuildingBlocks.EventSourcing;

public class AggregateBase : AggregateRoot<Guid>
{
    public Guid Id { get; protected set; }
    
    // For protecting the state, i.e. conflict prevention
    // The setter is only public for setting up test conditions
    public long Version { get; set; }
    
    [JsonIgnore]
    private readonly List<object> _uncommittedEvents = new();

    public IEnumerable<object> GetUncommittedEvents()
        => _uncommittedEvents;

    public void ClearUncommittedEvents()
        => _uncommittedEvents.Clear();

    protected void AddUncommittedEvent(object @event)
        => _uncommittedEvents.Add(@event);

    public virtual void Apply(IDomainEvent @event) {}
}

public interface IDomainEvent {}