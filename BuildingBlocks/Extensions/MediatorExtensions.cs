using BuildingBlocks.Ddd;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BuildingBlocks.Extensions;

public static class MediatorExtension
{
    public static async Task DispatchDomainEventsAsync<T>(this IMediator mediator, DbContext ctx) 
    {
        var domainEntities = ctx.ChangeTracker
            .Entries<IEntity>()
            .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());

        var domainEvents = domainEntities
            .SelectMany(x => x.Entity.DomainEvents)
            .ToList();

        domainEntities.ToList()
            .ForEach(entity => entity.Entity.ClearDomainEvents());

        var tasks = domainEvents
            .Select(async (domainEvent) => {
                await mediator.Publish(domainEvent).ConfigureAwait(false);
            });

        await Task.WhenAll(tasks).ConfigureAwait(false);
    }
}
