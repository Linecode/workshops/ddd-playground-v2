namespace MyVod.Common.BuildingBlocks.Extensions;

public static class EnumerableExtensions
{
    public static bool NotContains<T>(this IEnumerable<T> collection, T element)
        => !collection.Contains(element);
}
